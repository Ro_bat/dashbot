#!/bin/python
import asyncio

### Core class
from dashbot import Dashbot

def main():
    bot = Dashbot()
    bot.run()

if __name__ == '__main__':
    main()
