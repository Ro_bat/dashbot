import settings
from dashmodule import DashModule, DashAttribute
from dataclasses import User, Message, Context
import asyncio
import websockets
import functools
from dashbot import Dashbot

from typing import List

class StaticAttributeConnector(DashModule):
    settings = None

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        default_data = {'attributes':{}}
        self.settings = settings.Settings('static_attributes','dashbot',default_data)

    async def get_attribute(self, attribute_name : str) -> str:
        return self.settings.data['attributes'][attribute_name]

    def attributes(self) -> List[DashAttribute]:
        return [DashAttribute(d,functools.partial(self.get_attribute,d)) for d in self.settings.data['attributes'].keys()]
