import settings
from dashbot import DashAttributeError
from dashmodule import DashModule
from dataclasses import User, Message, Context
from .ds_death_detector import DarkSoulsDeathDetector
from .ds3_death_detector import DarkSouls3DeathDetector
import asyncio
import cv2

from typing import List, Callable, Awaitable
from numpy.typing import ArrayLike
from dashbot import Dashbot

class DeathDetectorConnector(DashModule):
    detector_ds = None
    settings = None

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        self.detector_ds = DarkSoulsDeathDetector()
        self.detector_ds3 = DarkSouls3DeathDetector()
        default_data_ds = {'score_threshold':10,'sleep_time':7}
        default_data_ds3 = {'score_threshold':10,'sleep_time':7}
        default_data = {'video_device':'/dev/video0','Dark Souls':default_data_ds,'Dark Souls III':default_data_ds3}
        self.settings = settings.Settings('DeathDetectorConnector','dashbot',default_data)

    async def detect_death(self, frame : ArrayLike) -> bool:
        try:
            game = await self.bot.attribute('game')
        except DashAttributeError:
            return False
        game = game[0]
        if game == 'Dark Souls':
            score = self.detector_ds.score_image(frame)
            return score < self.settings.data[game]['score_threshold']
        elif game == 'Dark Souls III':
            score = self.detector_ds3.score_image(frame)
            print(game, score)
            return score < self.settings.data[game]['score_threshold']
        else: # game not supported
            return False

    async def detect_death_loop(self) -> None:
        video_capture = cv2.VideoCapture(self.settings.data['video_device'])
        while True:
            await asyncio.sleep(0.03)
            ret, frame = video_capture.read()
            if frame is None:
                continue
            #score = self.detector.score_image(frame)
            death = await self.detect_death(frame)
            #print('score: ',score)
            if death:
                await asyncio.sleep(self.settings.data['sleep_time'])
                user = User(self.name,'internal',True)
                message = Message(user,'!deathadd_detector')
                context = Context(message,None,self)
                await self.bot.handle_context(context)

    def tasks(self) -> List[Callable[[],Awaitable[str]]]:
        return [self.detect_death_loop()]