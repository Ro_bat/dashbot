import numpy as np
import cv2
from progress.bar import IncrementalBar
import time

from numpy.typing import ArrayLike
from typing import List

class DarkSoulsDeathDetector:
    reference_image = None
    y_start = None
    x_start = None
    def convert(self,frame : ArrayLike) -> ArrayLike :
        return frame[:,:,2]-np.mean(frame[:,:,0:2],axis=2)
    def __init__(self) -> None:
        self.reference_image = self.convert(cv2.imread('resources/ds_youdied.png'))#youdied.png
        self.y_start = 336#300
        self.x_start = 344#300
    def score_image(self,image : ArrayLike) -> float:
        image_cropped = image[self.y_start:self.y_start+self.reference_image.shape[0],self.x_start:self.x_start+self.reference_image.shape[1],:]
        image_cropped = self.convert(image_cropped)
        diff = np.abs(image_cropped - self.reference_image)
        return float(np.mean(diff))
    def score_video(self, video : cv2.VideoCapture ,text : str = 'Processing') -> List[float]:
        scores = []
        bar = IncrementalBar(text, max=video.get(cv2.CAP_PROP_FRAME_COUNT))
        while(True):
            ret, frame = video.read()
            if ret == False:
                break
            scores.append(self.score_image(frame))
            bar.next()
        bar.finish()
        return scores
