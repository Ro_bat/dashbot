import settings
from dashmodule import DashModule
from dataclasses import User, Message, Context
import asyncio
import websockets
from typing import Callable, Awaitable, List, Tuple
from dashbot import Dashbot

class WebsocketConnector(DashModule):
    settings = None
    server_start = None

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        sample_credentials = {'sample_user':{'password':'password','mod':False}}
        default_data = {'listen_port':80
                       ,'host':'localhost'
                       ,'credentials':sample_credentials}
        self.settings = settings.Settings('websocket','dashbot',default_data)

    def check_credentials(self, user :str, password : str) -> Tuple[bool,bool]:
        credentials = self.settings.data['credentials']
        if user not in credentials:
            return False, False
        return credentials[user]['password'] == password, credentials[user]['mod']

    async def handle_websocket(self, websocket : websockets.WebSocketServerProtocol, path : str) -> None:
        path_parts = [d for d in path.split('/') if len(d)>0]
        if len(path_parts) != 2:
            await websocket.send('error')
            return
        user_name = path_parts[0]
        password = path_parts[1]

        password_ok, mod = self.check_credentials(user_name,password)
        if not password_ok:
            await websocket.send('fail_user')
            return
        command = await websocket.recv()
        #command = path + ' ' + arguments
        user = User(self.name,user_name,mod)
        message = Message(user,command)
        context = Context(message,None,self)
        await self.bot.handle_context(context)
        await websocket.send('success')

    async def run_server(self) -> None:
        self.server_start = await websockets.serve(self.handle_websocket, self.settings.data['host'], self.settings.data['listen_port'])
        await self.server_start.wait_closed()

    def tasks(self) -> List[Callable[[],Awaitable[str]]]:
        return [self.run_server()]