#!/bin/python
import settings
from dashbot import Dashbot
from dashmodule import DashModule, DashMethod
import random
from datetime import datetime
from dataclasses import Context
from typing import Dict, List

class QuoteModule(DashModule):
    settings = None
    sample_quote = {
        'text'
    }
    default_data = {'quotes':[],'default_context':'','auto_date_format':''}
    enabled = True

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        self.settings = settings.Settings('quote','dashbot',self.default_data)

    def strip(self, text: str) -> str:
        text = text.strip()
        if not text:
            return ''
        if text[0] == "'" and text[-1]=="'":
            text = text[1:-1]
        elif text[0] == '"' and text[-1]=='"':
            text = text[1:-1]
        return text

    def ordinal_suffix(self, day : int) -> str:
        suffix = ''
        dec = day % 10
        if 0 < dec < 4 and not 10 < (day%100) < 14:
            suffix = ["st", "nd", "rd"][dec - 1]
        else:
            suffix = 'th'
        return suffix
    
    def quote_object_to_string(self, quote : Dict) -> str:
        if quote['context']:
            return '{context}: "{quote}" ({game} - {date})'.format(context=quote['context'],quote=quote['text'],game=quote['game'],date=quote['date'])
        else:
            return '"{quote}" ({game} - {date})'.format(quote=quote['text'],game=quote['game'],date=quote['date'])

    def current_date(self) -> str:
        now = datetime.utcnow()
        date = now.strftime('%b %-d{daysuffix}, %Y UTC').format(daysuffix=self.ordinal_suffix(now.day))
        return date

    async def generate_quote(self,context : Context) -> Dict:
        arguments = context.arguments()
        if not arguments:
            return
        arguments = arguments.split('|')
        arguments = [self.strip(d) for d in arguments]
        
        if not 0 < len(arguments) < 4 or arguments[0] == ('--help') or not arguments[0]:
            # TODO print usage
            return None
        
        quote = arguments[0]
        speaker_context = None
        game = None
        date = None

        if len(arguments) > 1 and arguments[1]:
            speaker_context = arguments[1]
        else:
            speaker_context = self.settings.data['default_context']

        if len(arguments) > 2 and arguments[2]:
            game = arguments[2]
        else:
            game_attrib = await self.bot.attribute('game')
            if len(game_attrib)>0:
                game = game_attrib[0]
            else:
                await context.send('Could not determine game. No quote added.')
                return None
        
        if len(arguments) > 3 and arguments[3]:
            date = arguments[3]
        else:
            date = self.current_date()
        return {'text':quote,'context':speaker_context,'game':game,'date':date}

    async def addquote(self, context : Context) -> None:
        quote = await self.generate_quote(context)
        if quote is not None:
            self.settings.data['quotes'].append(quote)
            self.settings.save_data()
            await self.quote_by_number(context, -1)

    async def quote(self, context : Context) -> None:
        arguments = context.arguments()
        if not arguments:
            await self.quote_help(context)
        elif arguments[0] == '--help':
            # TODO print usage
            return
        else:
            try:
                num = int(arguments)-1
                if -1 <= num < len(self.settings.data['quotes']):
                    await self.quote_by_number(context,num)
                else:
                    await context.send('Quote number out of range. There are only {} quotes.'.format(len(self.settings.data['quotes'])))
                return
            except ValueError:
                await self.quote_by_text(context,arguments)
                return

    async def quote_help(self, context : Context) -> None:
        num_quotes = len(self.settings.data['quotes'])
        if num_quotes == 0:
            await context.send('There are no quotes.')
        else:
            random_num = int(random.random()*num_quotes)
            await self.quote_by_number(context, random_num)
    
    async def quote_by_text(self, context : Context, arguments : List[str]) -> None:
        quotes = [d for d in range(len(self.settings.data['quotes'])) if arguments in self.settings.data['quotes'][d]['text'].lower() ]
        if len(quotes) == 0:
            await context.send('No matching quote found.')
        else:
            await self.quote_by_number(context, random.choice(quotes))

    async def quote_by_number(self, context : Context ,number : int) -> None:
        readable_number = number + 1 if number >= 0 else len(self.settings.data['quotes'])+number +1
        quote = self.settings.data['quotes'][number]
        await context.send(str(readable_number)+'. '+self.quote_object_to_string(quote))

    async def addquote_preview(self, context : Context) -> None:
        quote = await self.generate_quote(context)
        if quote is not None:
            await context.send(self.quote_object_to_string(quote))

    async def delquote(self, context : Context) -> None:
        arguments = context.arguments()
        try:
            number = int(arguments)-1
            if 0 < number < len(self.settings.data['quotes']):
                await context.send('Deleting quote {num}: {text}'.format(num=number+1 ,text=self.quote_object_to_string(self.settings.data['quotes'][number])))
                self.settings.data['quotes'].pop(number)
                self.settings.save_data()
            else:
                await context.send('Quote number out of range. There are only {} quotes.'.format(len(self.settings.data['quotes'])))
        except ValueError:
            await context.send('That\'s, like, not a number. I really need the quote numbers for this, @{author}.'.format(author=context.message.author.name))
            return

    def methods(self) -> List[DashMethod]:
        return [DashMethod('quote'           , 'Prints a quote.'         , self.quote           )
               ,DashMethod('delquote'        , 'Deletes a quote.'        , self.delquote        ,requires_mod=True)
               ,DashMethod('addquote'        , 'Adds a quote.'           , self.addquote        ,requires_mod=True)
               ,DashMethod('addquote_preview', 'Preview for quote adding', self.addquote_preview)]