#!/bin/python
import settings
from dashbot import Dashbot
from dataclasses import Context, User
from dashmodule import DashModule, DashMethod
import random
from typing import List

class LurkModule(DashModule):
    settings = None
    default_data = {'replace_string':'<name>','lurk_messages':['<name> goes into lurk mode.']}
    enabled = True

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        self.settings = settings.Settings('lurk','dashbot',self.default_data)

    def random_lurk_message(self) -> str:
        return random.choice(self.settings.data['lurk_messages'])

    def full_lurk_message(self, author : User) -> str:
        return self.random_lurk_message().replace(self.settings.data['replace_string'],author.name)

    async def lurk(self,context : Context) -> None:
        await context.send_me(self.full_lurk_message(context.message.author))

    def methods(self) -> List[DashMethod]:
        return [DashMethod('lurk'  ,'Prints a lurk message.',self.lurk)]