#!/bin/python
from twitchio.ext import commands
import twitchio
import settings
from dashmodule import DashModule, DashAttribute
from dataclasses import User, Message, Context
import asyncio

from typing import Awaitable, List, Callable

class TwitchBot(commands.Bot):
    modules=None
    methods=[]
    message_callback=None

    def __init__(self, message_callback) -> None:
        default_data = {'target_channel':'','client_id':'','client_secret':'','nickname':'','oauth_token':'','prefix_token':'!','streaminfo_update_interval':10}
        self.twitch_settings = settings.Settings('twitch','dashbot',default_data)
        self.target_channel = self.twitch_settings.data['target_channel']
        super().__init__(irc_token=self.twitch_settings.data['oauth_token']
                        ,client_id=self.twitch_settings.data['client_id']
                        ,client_secret=self.twitch_settings.data['client_secret']
                        ,nick=self.twitch_settings.data['nickname']
                        ,prefix=self.twitch_settings.data['prefix_token']
                        ,initial_channels=[self.target_channel])
        self.modules = []
        self.methods = []
        self.message_callback=message_callback
    
    async def event_ready(self) -> None:
        print(f'Ready | {self.nick}')

    async def event_message(self, message : twitchio.Context) -> None:
        await self.message_callback(message)


class TwitchBotConnector(DashModule):
    twitch_bot = None
    attribute_game = None

    def __init__(self, bot) -> None:
        super().__init__(bot)
        self.twitch_bot = TwitchBot(self.handle_twitch_message)

    async def handle_twitch_message(self, message : twitchio.Context) -> None:
        user = User(message.author.name,self.name,message.author.is_mod)
        message = Message(user,message.content)
        context = Context(message, self.twitch_bot.target_channel, self)
        await self.bot.handle_context(context)

    async def send(self, message : str):
        await self.twitch_bot.get_channel(self.twitch_bot.target_channel).send(message)

    async def send_me(self, message : str):
        await self.twitch_bot.get_channel(self.twitch_bot.target_channel).send_me(message)

    def tasks(self) -> List[Callable[[],Awaitable[str]]]:
        return [self.twitch_bot.start(), self.streaminfo_update_loop()]

    async def current_game(self) -> str:
        return self.attribute_game

    async def streaminfo_update_loop(self) -> None:
        while True:
            try:
                await asyncio.sleep(float(self.twitch_bot.twitch_settings.data['streaminfo_update_interval']))
                channel = self.twitch_bot.target_channel
                stream_info = await self.twitch_bot.get_stream(channel)
                self.attribute_game = stream_info['game_name'] if stream_info else None
            except:
                await asyncio.sleep(10)

    def attributes(self) -> List[DashAttribute]:
        return [DashAttribute('game',self.current_game)]
