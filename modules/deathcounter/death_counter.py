#!/bin/python
import settings

from dataclasses import Context
from dashmodule import DashModule, DashMethod
from dashbot import DashAttributeError, Dashbot
from typing import List


class DeathCounter(DashModule):
    settings = None
    default_data = {}
    enabled = True

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        self.settings = settings.Settings('deathcount','dashbot',self.default_data)

    async def count(self) -> int:
        game_name = await self.bot.attribute('game')
        game_name = game_name[0]
        if game_name not in self.settings.data:
            self.settings.data[game_name] = 0
        return self.settings.data[game_name]


    def additional_text(self, number : int) -> str:
        if number == 420:
            return "Hehe, that's the weed number   ( ͡° ͜ʖ ͡°)"
        elif (number % 100) == 69:
            return 'Nice.'
        elif (number % 100) == 0:
            return 'Woo! 🎉'
        else:
            return 'Stay determined!'

    async def deathadd(self, context : Context) -> None:
        try:
            game_name = await self.bot.attribute('game')
            game_name = game_name[0]
            if game_name not in self.settings.data:
                self.settings.data[game_name] = 0
            self.settings.data[game_name] += 1
            count = await self.count()
            await context.send_me_all("Yay death! {} has died {} times. {}".format(context.channel,count,self.additional_text(count)))
            self.settings.save_data()
        except DashAttributeError as e:
            await context.send_me_all("Aw shucks. Could not determine {attribute}. Can't increase counter.".format(attribute=e.attribute))
            pass

    async def deathadd_detector(self, context : Context) -> None:
        if self.enabled:
            await self.deathadd(context)

    async def deaths(self, context : Context) -> None:
        try:
            await context.send_me_all("{} has died {} times.".format(context.channel,await self.count()))
        except DashAttributeError as e:
            await context.send_me_all("Aw shucks. Could not determine {attribute}. Can't get death count. Probably high though.".format(attribute=e.attribute))
            pass

    async def toggle_detector(self, context : Context) -> None:
        self.enabled = not self.enabled

    def methods(self) -> List[DashMethod]:
        return [DashMethod('deaths'           , 'Announces the number of deaths.'          ,self.deaths           )
               ,DashMethod('deathadd'         , 'Increments deaths by one.'                ,self.deathadd         , requires_mod=True)
               ,DashMethod('deathadd_detector', 'Increments deaths by one. Detector only.' ,self.deathadd_detector, requires_mod=True, listed=False)
               ,DashMethod('toggledetector'   , 'Toggles automatic death detection on/off.',self.toggle_detector  , requires_mod=True)]
