#!/bin/python
import settings
from dashbot import Dashbot
from dataclasses import Context
from dashmodule import DashModule, DashMethod
import random
import re
from typing import List

class GreetingsModule(DashModule):
    settings = None
    default_data = {'my_names':'[]','greetings':['hey']}
    enabled = True

    def __init__(self, bot : Dashbot) -> None:
        super().__init__(bot)
        self.settings = settings.Settings('greetings','dashbot',self.default_data)
        self.split_regex = '[!.,^\-\s\t]+'
        self.names = [d.lower() for d in self.settings.data['my_names']]
        self.greetings = [d.lower() for d in self.settings.data['greetings']]

    async def handle_message(self, context : Context) -> None:
        message_parts = re.split(self.split_regex,context.message.content)
        if len(message_parts) >= 2:
            if (message_parts[0].lower() in self.greetings) and (message_parts[1].lower() in self.names):
                await context.send(random.choice(self.greetings) + " @" + context.message.author.name + " ^^")
        return

    def methods(self) -> List[DashMethod]:
        return []