import asyncio
import settings
import importlib
from typing import List, Type

import dataclasses
import dashmodule


class DashAttributeError(Exception):
    attribute = None
    def __init__(self, attribute, message) -> None:
        self.attribute = attribute
        super().__init__(message)

class Dashbot():
    settings = None
    modules = []
    methods = []
    attribute_calls = {}
    
    def __init__(self) -> None:
        default_data = {'channel_name':'<name>','modules':[]}
        self.modules = []
        self.methods = []
        self.settings = settings.Settings('dashbot','dashbot',default_data)
        self.load_modules_from_settings()
        return

    def load_modules_from_settings(self) -> None:
        for module_name in self.settings.data['modules']:
            module = importlib.import_module(module_name)
            self.register_module(module.constructor)

    def register_module(self, module : Type[dashmodule.DashModule]) -> None:
        new_module = module(self)
        self.modules.append(new_module)
        print('Registering module: '+new_module.name())
        # register methods
        if len(new_module.methods()) > 0:
            print('  Methods: ' + ', '.join([d.name for d in new_module.methods()]))
            for method in new_module.methods():
                self.methods.append(method)
        # register attributes
        if len(new_module.attributes()) > 0:
            print('  Attributes: ' + ', '.join([d.name for d in new_module.attributes()]))
            for attribute in new_module.attributes():
                if attribute.name in self.attribute_calls:
                    self.attribute_calls[attribute.name].append(attribute.call)
                else:
                    self.attribute_calls[attribute.name] = [attribute.call]

    async def handle_context(self, context : dataclasses.Context) -> None:
        context.bot = self
        context.channel = self.settings.data['channel_name']
        if context.message.content.startswith('!'):
            message_parts = context.message.content.split(' ')
            command = message_parts[0][1:]
            for method in self.methods:
                if command == method.name:
                    if not method.requires_mod or context.message.author.elevated_rights:
                        await method.call(context)
        else:
            for module in self.modules:
                await module.handle_message(context)
    
    async def send(self, message : str) -> None:
        for module in self.modules:
            await module.send(message)

    async def send_me(self, message : str) -> None:
        for module in self.modules:
            await module.send_me(message)

    async def attribute(self,name : str) -> List[str]:
        if name in self.attribute_calls:
            attributes = [await d() for d in self.attribute_calls[name]]
            attributes = [d for d in attributes if d is not None]
            if len(attributes)>0:
                return attributes
            else:
                raise DashAttributeError(name, 'Could not determine: {}'.format(name))
        else:
            raise DashAttributeError(name, 'Could not determine: {}'.format(name))

    def run(self) -> None:
        loop = asyncio.get_event_loop()
        for module in self.modules:
            for task in module.tasks():
                loop.create_task(task)
        loop.run_forever()
        return
