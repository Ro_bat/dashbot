from dashmodule import DashModule

class User():
    name = ''
    platform = ''
    elevated_rights = False
    def __init__(self,name : str,platform : str ,elevated_rights : bool) -> None:
        self.name = name
        self.platform = platform
        self.elevated_rights = elevated_rights

class Message():
    author = None
    content = None
    def __init__(self,author : User,content : str) -> None:
        self.author = author
        self.content = content

class Context():
    message = None
    channel = None
    module = None
    bot = None
    additional_info = None
    def __init__(self, message : Message, channel : str, module : DashModule) -> None:
        self.message = message
        self.channel = channel
        self.module = module
        self.additional_info = {}

    async def send(self, message : str) -> None:
        if self.module is not None:
            await self.module.send(message)

    async def send_me(self, message : str) -> None:
        if self.module is not None:
            await self.module.send_me(message)

    async def send_all(self,message : str) -> None:
        await self.bot.send(message)

    async def send_me_all(self,message : str) -> None:
        await self.bot.send_me(message)

    def arguments(self) -> str:
        if self.message.content.startswith('!'):
            message_parts = self.message.content.split(' ')
            return ' '.join(message_parts[1:])
        else:
            return ''
