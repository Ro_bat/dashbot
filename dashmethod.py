class DashMethod():
    name = None
    call = None
    description = None
    requires_mod = None
    listed = None

    def __init__(self, name, description, method, requires_mod=False, listed=True) -> None:
        self.name = name
        self.description = description
        self.requires_mod = requires_mod
        self.call = method
        self.listed = listed
