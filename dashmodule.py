#!/bin/python
from typing import List, Type
from collections.abc import Callable, Awaitable

from dashattribute import DashAttribute
from dashmethod import DashMethod

class DashModule():
    bot = None
    def __init__(self, bot) -> None:
        self.bot = bot

    def name(self) -> str:
        return self.__class__.__name__

    def methods(self) -> List[DashMethod]:
        return []

    async def handle_message(self, context) -> None:
        return

    def tasks(self) -> List[Callable[[],Awaitable[str]]]:
        return []

    def attributes(self) -> List[DashAttribute]:
        return []

    async def send(self, message : str) -> None:
        return

    async def send_me(self, message : str) -> None:
        return
