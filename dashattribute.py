class DashAttribute():
    name = None
    call = None

    def __init__(self, name, call) -> None:
        self.name = name
        self.call = call
